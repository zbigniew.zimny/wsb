﻿namespace BMICalculator
{
    partial class BMICalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BMICalculator));
            this.genderPanel = new System.Windows.Forms.Panel();
            this.maleRadioButton = new System.Windows.Forms.RadioButton();
            this.femaleRadioButton = new System.Windows.Forms.RadioButton();
            this.formPanel = new System.Windows.Forms.Panel();
            this.bmiButton = new System.Windows.Forms.Button();
            this.height = new System.Windows.Forms.TextBox();
            this.weight = new System.Windows.Forms.TextBox();
            this.weightLabel = new System.Windows.Forms.Label();
            this.heightLabel = new System.Windows.Forms.Label();
            this.weightUnitLabel = new System.Windows.Forms.Label();
            this.heightUnitLabel = new System.Windows.Forms.Label();
            this.resultPanel = new System.Windows.Forms.Panel();
            this.bmiPictureBox = new System.Windows.Forms.PictureBox();
            this.resultScoreLabel = new System.Windows.Forms.Label();
            this.resultLabel = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.titlePanel = new System.Windows.Forms.Panel();
            this.bmiTitleLabel = new System.Windows.Forms.Label();
            this.genderPanel.SuspendLayout();
            this.formPanel.SuspendLayout();
            this.resultPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bmiPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.titlePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // genderPanel
            // 
            this.genderPanel.BackColor = System.Drawing.SystemColors.Control;
            this.genderPanel.Controls.Add(this.maleRadioButton);
            this.genderPanel.Controls.Add(this.femaleRadioButton);
            this.genderPanel.Location = new System.Drawing.Point(0, 217);
            this.genderPanel.Name = "genderPanel";
            this.genderPanel.Size = new System.Drawing.Size(663, 52);
            this.genderPanel.TabIndex = 0;
            this.genderPanel.TabStop = true;
            // 
            // maleRadioButton
            // 
            this.maleRadioButton.AutoSize = true;
            this.maleRadioButton.Font = new System.Drawing.Font("Jura", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.maleRadioButton.Location = new System.Drawing.Point(409, 14);
            this.maleRadioButton.Name = "maleRadioButton";
            this.maleRadioButton.Size = new System.Drawing.Size(131, 24);
            this.maleRadioButton.TabIndex = 2;
            this.maleRadioButton.TabStop = true;
            this.maleRadioButton.Text = "Mężczyzna";
            this.maleRadioButton.UseVisualStyleBackColor = true;
            // 
            // femaleRadioButton
            // 
            this.femaleRadioButton.AutoSize = true;
            this.femaleRadioButton.Font = new System.Drawing.Font("Jura", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.femaleRadioButton.Location = new System.Drawing.Point(155, 14);
            this.femaleRadioButton.Name = "femaleRadioButton";
            this.femaleRadioButton.Size = new System.Drawing.Size(95, 24);
            this.femaleRadioButton.TabIndex = 1;
            this.femaleRadioButton.TabStop = true;
            this.femaleRadioButton.Text = "Kobieta";
            this.femaleRadioButton.UseVisualStyleBackColor = true;
            // 
            // formPanel
            // 
            this.formPanel.BackColor = System.Drawing.Color.LightGray;
            this.formPanel.Controls.Add(this.bmiButton);
            this.formPanel.Controls.Add(this.height);
            this.formPanel.Controls.Add(this.weight);
            this.formPanel.Controls.Add(this.weightLabel);
            this.formPanel.Controls.Add(this.heightLabel);
            this.formPanel.Controls.Add(this.weightUnitLabel);
            this.formPanel.Controls.Add(this.heightUnitLabel);
            this.formPanel.Location = new System.Drawing.Point(0, 275);
            this.formPanel.Name = "formPanel";
            this.formPanel.Size = new System.Drawing.Size(663, 312);
            this.formPanel.TabIndex = 3;
            // 
            // bmiButton
            // 
            this.bmiButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bmiButton.BackColor = System.Drawing.Color.SeaGreen;
            this.bmiButton.Font = new System.Drawing.Font("Jura", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bmiButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.bmiButton.Location = new System.Drawing.Point(155, 223);
            this.bmiButton.Name = "bmiButton";
            this.bmiButton.Padding = new System.Windows.Forms.Padding(2);
            this.bmiButton.Size = new System.Drawing.Size(336, 50);
            this.bmiButton.TabIndex = 6;
            this.bmiButton.Text = "Oblicz BMI";
            this.bmiButton.UseVisualStyleBackColor = false;
            this.bmiButton.Click += new System.EventHandler(this.BmiButton_Click);
            // 
            // height
            // 
            this.height.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.height.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.height.Location = new System.Drawing.Point(238, 141);
            this.height.MaxLength = 3;
            this.height.Name = "height";
            this.height.Size = new System.Drawing.Size(200, 30);
            this.height.TabIndex = 5;
            this.height.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.height.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateEnteredValue);
            // 
            // weight
            // 
            this.weight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.weight.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.weight.Location = new System.Drawing.Point(238, 52);
            this.weight.MaxLength = 3;
            this.weight.Name = "weight";
            this.weight.Size = new System.Drawing.Size(200, 30);
            this.weight.TabIndex = 4;
            this.weight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.weight.Validating += new System.ComponentModel.CancelEventHandler(this.ValidateEnteredValue);
            // 
            // weightLabel
            // 
            this.weightLabel.AutoSize = true;
            this.weightLabel.Font = new System.Drawing.Font("Jura", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.weightLabel.Location = new System.Drawing.Point(111, 55);
            this.weightLabel.Name = "weightLabel";
            this.weightLabel.Size = new System.Drawing.Size(82, 27);
            this.weightLabel.TabIndex = 3;
            this.weightLabel.Text = "Waga";
            // 
            // heightLabel
            // 
            this.heightLabel.AutoSize = true;
            this.heightLabel.Font = new System.Drawing.Font("Jura", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.heightLabel.Location = new System.Drawing.Point(111, 144);
            this.heightLabel.Name = "heightLabel";
            this.heightLabel.Size = new System.Drawing.Size(101, 27);
            this.heightLabel.TabIndex = 4;
            this.heightLabel.Text = "Wzrost";
            // 
            // weightUnitLabel
            // 
            this.weightUnitLabel.AutoSize = true;
            this.weightUnitLabel.Font = new System.Drawing.Font("Jura", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.weightUnitLabel.Location = new System.Drawing.Point(477, 52);
            this.weightUnitLabel.Name = "weightUnitLabel";
            this.weightUnitLabel.Size = new System.Drawing.Size(40, 27);
            this.weightUnitLabel.TabIndex = 5;
            this.weightUnitLabel.Text = "kg";
            // 
            // heightUnitLabel
            // 
            this.heightUnitLabel.AutoSize = true;
            this.heightUnitLabel.Font = new System.Drawing.Font("Jura", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.heightUnitLabel.Location = new System.Drawing.Point(477, 144);
            this.heightUnitLabel.Name = "heightUnitLabel";
            this.heightUnitLabel.Size = new System.Drawing.Size(49, 27);
            this.heightUnitLabel.TabIndex = 6;
            this.heightUnitLabel.Text = "cm";
            // 
            // resultPanel
            // 
            this.resultPanel.Controls.Add(this.bmiPictureBox);
            this.resultPanel.Controls.Add(this.resultScoreLabel);
            this.resultPanel.Controls.Add(this.resultLabel);
            this.resultPanel.Location = new System.Drawing.Point(-1, 603);
            this.resultPanel.Name = "resultPanel";
            this.resultPanel.Size = new System.Drawing.Size(663, 235);
            this.resultPanel.TabIndex = 0;
            // 
            // bmiPictureBox
            // 
            this.bmiPictureBox.Image = global::BMICalculator.Properties.Resources.bmi_zakresy;
            this.bmiPictureBox.Location = new System.Drawing.Point(3, 81);
            this.bmiPictureBox.Name = "bmiPictureBox";
            this.bmiPictureBox.Size = new System.Drawing.Size(661, 154);
            this.bmiPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bmiPictureBox.TabIndex = 0;
            this.bmiPictureBox.TabStop = false;
            // 
            // resultScoreLabel
            // 
            this.resultScoreLabel.AutoSize = true;
            this.resultScoreLabel.Font = new System.Drawing.Font("Jura", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.resultScoreLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.resultScoreLabel.Location = new System.Drawing.Point(453, 14);
            this.resultScoreLabel.Name = "resultScoreLabel";
            this.resultScoreLabel.Size = new System.Drawing.Size(0, 40);
            this.resultScoreLabel.TabIndex = 1;
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.Font = new System.Drawing.Font("Jura", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.resultLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.resultLabel.Location = new System.Drawing.Point(121, 21);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(311, 33);
            this.resultLabel.TabIndex = 2;
            this.resultLabel.Text = "Twoje BMI wynosi: ";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // titlePanel
            // 
            this.titlePanel.BackColor = System.Drawing.SystemColors.Control;
            this.titlePanel.BackgroundImage = global::BMICalculator.Properties.Resources.bmi_icon;
            this.titlePanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.titlePanel.Controls.Add(this.bmiTitleLabel);
            this.titlePanel.Cursor = System.Windows.Forms.Cursors.Default;
            this.titlePanel.Location = new System.Drawing.Point(-2, -3);
            this.titlePanel.Name = "titlePanel";
            this.titlePanel.Size = new System.Drawing.Size(664, 214);
            this.titlePanel.TabIndex = 3;
            // 
            // bmiTitleLabel
            // 
            this.bmiTitleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bmiTitleLabel.AutoSize = true;
            this.bmiTitleLabel.BackColor = System.Drawing.Color.DimGray;
            this.bmiTitleLabel.Font = new System.Drawing.Font("Jura", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bmiTitleLabel.ForeColor = System.Drawing.Color.AliceBlue;
            this.bmiTitleLabel.Location = new System.Drawing.Point(160, 78);
            this.bmiTitleLabel.Name = "bmiTitleLabel";
            this.bmiTitleLabel.Padding = new System.Windows.Forms.Padding(2);
            this.bmiTitleLabel.Size = new System.Drawing.Size(382, 59);
            this.bmiTitleLabel.TabIndex = 0;
            this.bmiTitleLabel.Text = "Kalkulator BMI";
            this.bmiTitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BMICalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(660, 836);
            this.Controls.Add(this.resultPanel);
            this.Controls.Add(this.formPanel);
            this.Controls.Add(this.genderPanel);
            this.Controls.Add(this.titlePanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BMICalculator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BMI Calculator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BMICalculator_FormClosing);
            this.genderPanel.ResumeLayout(false);
            this.genderPanel.PerformLayout();
            this.formPanel.ResumeLayout(false);
            this.formPanel.PerformLayout();
            this.resultPanel.ResumeLayout(false);
            this.resultPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bmiPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.titlePanel.ResumeLayout(false);
            this.titlePanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel titlePanel;
        private System.Windows.Forms.Label bmiTitleLabel;
        private System.Windows.Forms.Panel genderPanel;
        private System.Windows.Forms.RadioButton maleRadioButton;
        private System.Windows.Forms.RadioButton femaleRadioButton;
        private System.Windows.Forms.Panel formPanel;
        private System.Windows.Forms.Label weightLabel;
        private System.Windows.Forms.Label heightLabel;
        private System.Windows.Forms.Label weightUnitLabel;
        private System.Windows.Forms.Label heightUnitLabel;
        private System.Windows.Forms.TextBox weight;
        private System.Windows.Forms.TextBox height;
        private System.Windows.Forms.Button bmiButton;
        private System.Windows.Forms.Panel resultPanel;
        private System.Windows.Forms.Label resultScoreLabel;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.PictureBox bmiPictureBox;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}

