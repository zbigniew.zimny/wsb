﻿using System;
using System.Management;

namespace CPU
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend7 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series18 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend8 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series19 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series20 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series21 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend9 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series22 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series23 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series24 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.pTemp = new System.Diagnostics.PerformanceCounter();
            this.pCPU = new System.Diagnostics.PerformanceCounter();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.metroProgressBarCPU = new MetroFramework.Controls.MetroProgressBar();
            this.labelProcesor = new MetroFramework.Controls.MetroLabel();
            this.wskaznikCPU = new MetroFramework.Controls.MetroLabel();
            this.wskaznikTemp = new MetroFramework.Controls.MetroLabel();
            this.labelTemp = new MetroFramework.Controls.MetroLabel();
            this.metroProgressBarTemp = new MetroFramework.Controls.MetroProgressBar();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.wskaznik_cpu_min = new MetroFramework.Controls.MetroLabel();
            this.wskaznik_temp_min = new MetroFramework.Controls.MetroLabel();
            this.wskaznik_temp_avg = new MetroFramework.Controls.MetroLabel();
            this.wskaznik_temp_max = new MetroFramework.Controls.MetroLabel();
            this.wskaznik_cpu_avg = new MetroFramework.Controls.MetroLabel();
            this.wskaznik_cpu_max = new MetroFramework.Controls.MetroLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.wskaznik_czas = new MetroFramework.Controls.MetroLabel();
            this.label11 = new System.Windows.Forms.Label();
            this.wskaznik_ilosc_pomiarow = new MetroFramework.Controls.MetroLabel();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pCPU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            this.SuspendLayout();
            // 
            // pTemp
            // 
            this.pTemp.CategoryName = "Thermal Zone Information";
            this.pTemp.CounterName = "Temperature";
            //this.pTemp.InstanceName = "\\_TZ.TZ01";

            ManagementObjectSearcher searcher = new ManagementObjectSearcher(@"root\WMI", "SELECT * FROM MSAcpi_ThermalZoneTemperature");

            foreach (ManagementObject obj in searcher.Get())
            {
                String instanceName  = obj["InstanceName"].ToString();
                String instanceNameAfterSlash = instanceName.Substring(instanceName.LastIndexOf("\\") + 1);
                this.pTemp.InstanceName = "\\_TZ." + instanceNameAfterSlash.Substring(0, instanceNameAfterSlash.LastIndexOf("_"));
            }

            // 
            // pCPU
            // 
            this.pCPU.CategoryName = "Processor";
            this.pCPU.CounterName = "% Processor Time";
            this.pCPU.InstanceName = "_Total";
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // metroProgressBarCPU
            // 
            this.metroProgressBarCPU.Location = new System.Drawing.Point(200, 100);
            this.metroProgressBarCPU.Margin = new System.Windows.Forms.Padding(4);
            this.metroProgressBarCPU.Name = "metroProgressBarCPU";
            this.metroProgressBarCPU.Size = new System.Drawing.Size(750, 80);
            this.metroProgressBarCPU.TabIndex = 1;
            // 
            // labelProcesor
            // 
            this.labelProcesor.AutoSize = true;
            this.labelProcesor.Location = new System.Drawing.Point(30, 130);
            this.labelProcesor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelProcesor.Name = "labelProcesor";
            this.labelProcesor.Size = new System.Drawing.Size(107, 19);
            this.labelProcesor.TabIndex = 2;
            this.labelProcesor.Text = "Obciążenie CPU:";
            this.labelProcesor.Click += new System.EventHandler(this.labelProcesor_Click);
            // 
            // wskaznikCPU
            // 
            this.wskaznikCPU.AutoSize = true;
            this.wskaznikCPU.Location = new System.Drawing.Point(958, 130);
            this.wskaznikCPU.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wskaznikCPU.Name = "wskaznikCPU";
            this.wskaznikCPU.Size = new System.Drawing.Size(31, 19);
            this.wskaznikCPU.TabIndex = 3;
            this.wskaznikCPU.Text = "0 %";
            this.wskaznikCPU.Click += new System.EventHandler(this.metroLabel2_Click);
            // 
            // wskaznikTemp
            // 
            this.wskaznikTemp.AutoSize = true;
            this.wskaznikTemp.Location = new System.Drawing.Point(958, 230);
            this.wskaznikTemp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wskaznikTemp.Name = "wskaznikTemp";
            this.wskaznikTemp.Size = new System.Drawing.Size(34, 19);
            this.wskaznikTemp.TabIndex = 6;
            this.wskaznikTemp.Text = "0 °C";
            this.wskaznikTemp.Click += new System.EventHandler(this.wskaznikTemp_Click);
            // 
            // labelTemp
            // 
            this.labelTemp.AutoSize = true;
            this.labelTemp.Location = new System.Drawing.Point(30, 230);
            this.labelTemp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTemp.Name = "labelTemp";
            this.labelTemp.Size = new System.Drawing.Size(86, 19);
            this.labelTemp.TabIndex = 5;
            this.labelTemp.Text = "Temperatura:";
            this.labelTemp.Click += new System.EventHandler(this.metroLabel3_Click);
            // 
            // metroProgressBarTemp
            // 
            this.metroProgressBarTemp.Location = new System.Drawing.Point(200, 200);
            this.metroProgressBarTemp.Margin = new System.Windows.Forms.Padding(4);
            this.metroProgressBarTemp.Name = "metroProgressBarTemp";
            this.metroProgressBarTemp.Size = new System.Drawing.Size(750, 80);
            this.metroProgressBarTemp.TabIndex = 4;
            // 
            // chart1
            // 
            chartArea7.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea7);
            legend7.Name = "Legend1";
            this.chart1.Legends.Add(legend7);
            this.chart1.Location = new System.Drawing.Point(130, 331);
            this.chart1.Margin = new System.Windows.Forms.Padding(4);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series17.ChartArea = "ChartArea1";
            series17.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series17.Legend = "Legend1";
            series17.Name = "CPU";
            series18.ChartArea = "ChartArea1";
            series18.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series18.Legend = "Legend1";
            series18.Name = "Temp";
            this.chart1.Series.Add(series17);
            this.chart1.Series.Add(series18);
            this.chart1.Size = new System.Drawing.Size(950, 220);
            this.chart1.TabIndex = 7;
            this.chart1.Click += new System.EventHandler(this.chart1_Click);
            // 
            // chart2
            // 
            chartArea8.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea8);
            legend8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            legend8.IsTextAutoFit = false;
            legend8.Name = "Legend1";
            this.chart2.Legends.Add(legend8);
            this.chart2.Location = new System.Drawing.Point(150, 600);
            this.chart2.Margin = new System.Windows.Forms.Padding(4);
            this.chart2.Name = "chart2";
            series19.ChartArea = "ChartArea1";
            series19.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series19.Legend = "Legend1";
            series19.Name = "Najniższa temperatura";
            series20.ChartArea = "ChartArea1";
            series20.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series20.Legend = "Legend1";
            series20.Name = "Średnia temperatura";
            series21.ChartArea = "ChartArea1";
            series21.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series21.Legend = "Legend1";
            series21.Name = "Najwyższa temperatura";
            this.chart2.Series.Add(series19);
            this.chart2.Series.Add(series20);
            this.chart2.Series.Add(series21);
            this.chart2.Size = new System.Drawing.Size(750, 150);
            this.chart2.TabIndex = 8;
            // 
            // chart3
            // 
            chartArea9.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea9);
            legend9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            legend9.IsTextAutoFit = false;
            legend9.Name = "Legend1";
            this.chart3.Legends.Add(legend9);
            this.chart3.Location = new System.Drawing.Point(150, 800);
            this.chart3.Margin = new System.Windows.Forms.Padding(4);
            this.chart3.Name = "chart3";
            this.chart3.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Pastel;
            series22.ChartArea = "ChartArea1";
            series22.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series22.Legend = "Legend1";
            series22.Name = "Minimalne obciążenie CPU";
            series23.ChartArea = "ChartArea1";
            series23.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series23.Legend = "Legend1";
            series23.Name = "Średnie obciążenie CPU";
            series24.ChartArea = "ChartArea1";
            series24.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series24.Legend = "Legend1";
            series24.Name = "Maksymalne obciążenie CPU";
            this.chart3.Series.Add(series22);
            this.chart3.Series.Add(series23);
            this.chart3.Series.Add(series24);
            this.chart3.Size = new System.Drawing.Size(800, 150);
            this.chart3.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(217, 579);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Wynik pomiaru temperatury:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(217, 779);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(245, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Wynik pomiaru obciążenia procesora:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // wskaznik_cpu_min
            // 
            this.wskaznik_cpu_min.AutoSize = true;
            this.wskaznik_cpu_min.Location = new System.Drawing.Point(923, 810);
            this.wskaznik_cpu_min.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wskaznik_cpu_min.Name = "wskaznik_cpu_min";
            this.wskaznik_cpu_min.Size = new System.Drawing.Size(31, 19);
            this.wskaznik_cpu_min.TabIndex = 12;
            this.wskaznik_cpu_min.Text = "0 %";
            // 
            // wskaznik_temp_min
            // 
            this.wskaznik_temp_min.AutoSize = true;
            this.wskaznik_temp_min.Location = new System.Drawing.Point(920, 610);
            this.wskaznik_temp_min.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wskaznik_temp_min.Name = "wskaznik_temp_min";
            this.wskaznik_temp_min.Size = new System.Drawing.Size(34, 19);
            this.wskaznik_temp_min.TabIndex = 14;
            this.wskaznik_temp_min.Text = "0 °C";
            // 
            // wskaznik_temp_avg
            // 
            this.wskaznik_temp_avg.AutoSize = true;
            this.wskaznik_temp_avg.Location = new System.Drawing.Point(920, 630);
            this.wskaznik_temp_avg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wskaznik_temp_avg.Name = "wskaznik_temp_avg";
            this.wskaznik_temp_avg.Size = new System.Drawing.Size(34, 19);
            this.wskaznik_temp_avg.TabIndex = 15;
            this.wskaznik_temp_avg.Text = "0 °C";
            // 
            // wskaznik_temp_max
            // 
            this.wskaznik_temp_max.AutoSize = true;
            this.wskaznik_temp_max.Location = new System.Drawing.Point(920, 650);
            this.wskaznik_temp_max.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wskaznik_temp_max.Name = "wskaznik_temp_max";
            this.wskaznik_temp_max.Size = new System.Drawing.Size(34, 19);
            this.wskaznik_temp_max.TabIndex = 16;
            this.wskaznik_temp_max.Text = "0 °C";
            // 
            // wskaznik_cpu_avg
            // 
            this.wskaznik_cpu_avg.AutoSize = true;
            this.wskaznik_cpu_avg.Location = new System.Drawing.Point(923, 829);
            this.wskaznik_cpu_avg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wskaznik_cpu_avg.Name = "wskaznik_cpu_avg";
            this.wskaznik_cpu_avg.Size = new System.Drawing.Size(31, 19);
            this.wskaznik_cpu_avg.TabIndex = 17;
            this.wskaznik_cpu_avg.Text = "0 %";
            // 
            // wskaznik_cpu_max
            // 
            this.wskaznik_cpu_max.AutoSize = true;
            this.wskaznik_cpu_max.Location = new System.Drawing.Point(923, 850);
            this.wskaznik_cpu_max.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wskaznik_cpu_max.Name = "wskaznik_cpu_max";
            this.wskaznik_cpu_max.Size = new System.Drawing.Size(31, 19);
            this.wskaznik_cpu_max.TabIndex = 18;
            this.wskaznik_cpu_max.Text = "0 %";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(217, 310);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(475, 17);
            this.label3.TabIndex = 19;
            this.label3.Text = "Pomiar % łącznego użycia procesora i temperatury w czasie rzeczywistym:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(217, 984);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 17);
            this.label4.TabIndex = 20;
            this.label4.Text = "Czas trwania pomiaru:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.ForeColor = System.Drawing.Color.Green;
            this.label5.Location = new System.Drawing.Point(127, 340);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 17);
            this.label5.TabIndex = 21;
            this.label5.Text = "(%)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.ForeColor = System.Drawing.Color.MediumBlue;
            this.label6.Location = new System.Drawing.Point(127, 357);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 17);
            this.label6.TabIndex = 22;
            this.label6.Text = "(°C)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(127, 610);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 17);
            this.label7.TabIndex = 23;
            this.label7.Text = "(°C)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(130, 810);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 17);
            this.label8.TabIndex = 24;
            this.label8.Text = "(%)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(651, 723);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 17);
            this.label9.TabIndex = 25;
            this.label9.Text = "(T)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(651, 922);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 17);
            this.label10.TabIndex = 26;
            this.label10.Text = "(T)";
            // 
            // wskaznik_czas
            // 
            this.wskaznik_czas.AutoSize = true;
            this.wskaznik_czas.Location = new System.Drawing.Point(421, 984);
            this.wskaznik_czas.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wskaznik_czas.Name = "wskaznik_czas";
            this.wskaznik_czas.Size = new System.Drawing.Size(57, 19);
            this.wskaznik_czas.TabIndex = 27;
            this.wskaznik_czas.Text = "00:00:00";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(571, 986);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(185, 17);
            this.label11.TabIndex = 28;
            this.label11.Text = "Ilość wykonanych pomiarów:";
            // 
            // wskaznik_ilosc_pomiarow
            // 
            this.wskaznik_ilosc_pomiarow.AutoSize = true;
            this.wskaznik_ilosc_pomiarow.Location = new System.Drawing.Point(794, 984);
            this.wskaznik_ilosc_pomiarow.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wskaznik_ilosc_pomiarow.Name = "wskaznik_ilosc_pomiarow";
            this.wskaznik_ilosc_pomiarow.Size = new System.Drawing.Size(16, 19);
            this.wskaznik_ilosc_pomiarow.TabIndex = 29;
            this.wskaznik_ilosc_pomiarow.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(991, 521);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 17);
            this.label12.TabIndex = 30;
            this.label12.Text = "(T)";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1150, 1041);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.wskaznik_ilosc_pomiarow);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.wskaznik_czas);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.wskaznik_cpu_max);
            this.Controls.Add(this.wskaznik_cpu_avg);
            this.Controls.Add(this.wskaznik_temp_max);
            this.Controls.Add(this.wskaznik_temp_avg);
            this.Controls.Add(this.wskaznik_temp_min);
            this.Controls.Add(this.wskaznik_cpu_min);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chart3);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.wskaznikTemp);
            this.Controls.Add(this.labelTemp);
            this.Controls.Add(this.metroProgressBarTemp);
            this.Controls.Add(this.wskaznikCPU);
            this.Controls.Add(this.labelProcesor);
            this.Controls.Add(this.metroProgressBarCPU);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(26, 75, 26, 25);
            this.Text = "Pomiar obciążenia procesora i temperatury:";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pCPU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Diagnostics.PerformanceCounter pTemp;
        private System.Diagnostics.PerformanceCounter pCPU;
        private System.Windows.Forms.Timer timer;
        private MetroFramework.Controls.MetroProgressBar metroProgressBarCPU;
        private MetroFramework.Controls.MetroLabel labelProcesor;
        private MetroFramework.Controls.MetroLabel wskaznikCPU;
        private MetroFramework.Controls.MetroLabel wskaznikTemp;
        private MetroFramework.Controls.MetroLabel labelTemp;
        private MetroFramework.Controls.MetroProgressBar metroProgressBarTemp;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroLabel wskaznik_cpu_min;
        private MetroFramework.Controls.MetroLabel wskaznik_temp_min;
        private MetroFramework.Controls.MetroLabel wskaznik_temp_avg;
        private MetroFramework.Controls.MetroLabel wskaznik_temp_max;
        private MetroFramework.Controls.MetroLabel wskaznik_cpu_avg;
        private MetroFramework.Controls.MetroLabel wskaznik_cpu_max;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private MetroFramework.Controls.MetroLabel wskaznik_czas;
        private System.Windows.Forms.Label label11;
        private MetroFramework.Controls.MetroLabel wskaznik_ilosc_pomiarow;
        private System.Windows.Forms.Label label12;
    }
}

