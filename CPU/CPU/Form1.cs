﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CPU
{
    public partial class Form1 : MetroFramework.Forms.MetroForm
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            timer.Start();
        }

        List<double> fcpuList = new List<double>();
        List<double> ftempList = new List<double>();

        int h=0, m=0, s=0, count=0;
        private void timer_Tick(object sender, EventArgs e)
        {

            double fcpu = pCPU.NextValue();
            double ftemp = pTemp.NextValue() -273;

            if (fcpu != 0)
            {
                fcpuList.Add(fcpu);
                ftempList.Add(ftemp);

                double fcpu_min = fcpuList.Min();
                double fcpu_max = fcpuList.Max();
                double fcpu_avg = fcpuList.Average();

                double ftemp_min = ftempList.Min();
                double ftemp_max = ftempList.Max();
                double ftemp_avg = ftempList.Average();

                //Ustawianie wartości dla CPU i temperatury:
                metroProgressBarCPU.Value = (int)fcpu;
                metroProgressBarTemp.Value = (int)ftemp;

                //Aktualizacja wskaźnika:
                wskaznikCPU.Text = string.Format("{0:0.00}%", fcpu);
                wskaznikTemp.Text = string.Format("{0:0}°C", ftemp);


                //Wykres:
                chart1.Series["CPU"].Points.AddY(fcpu);
                chart1.Series["Temp"].Points.AddY(ftemp);


                //Wykres - podsumowanie temperatur:
                chart2.Series["Najniższa temperatura"].Points.AddY(ftemp_min);
                chart2.Series["Średnia temperatura"].Points.AddY(ftemp_avg);
                chart2.Series["Najwyższa temperatura"].Points.AddY(ftemp_max);

                //Aktualizacja wskaźnika:
                wskaznik_temp_min.Text = string.Format("{0:0}°C", ftemp_min);
                wskaznik_temp_avg.Text = string.Format("{0:0}°C", ftemp_avg);
                wskaznik_temp_max.Text = string.Format("{0:0}°C", ftemp_max);

                //Wykres - podsumowanie CPU:
                chart3.Series["Minimalne obciążenie CPU"].Points.AddY(fcpu_min);
                chart3.Series["Średnie obciążenie CPU"].Points.AddY(fcpu_avg);
                chart3.Series["Maksymalne obciążenie CPU"].Points.AddY(fcpu_max);

                //Aktualizacja wskaźnika:
                wskaznik_cpu_min.Text = string.Format("{0:0.00}%", fcpu_min);
                wskaznik_cpu_avg.Text = string.Format("{0:0.00}%", fcpu_avg);
                wskaznik_cpu_max.Text = string.Format("{0:0.00}%", fcpu_max);



                //Czas od uruchomienia


                count += 1;
                s += 1;
                if (s == 60)
                {
                    s = 0;
                    m += 1;
                }
                if (m == 60)
                {
                    m = 0;
                    h += 1;
                }
                //Aktualizacja textbox
                wskaznik_czas.Text = string.Format("{0}:{1}:{2}", h.ToString().PadLeft(2, '0'), m.ToString().PadLeft(2, '0'), s.ToString().PadLeft(2, '0'));
                wskaznik_ilosc_pomiarow.Text = string.Format("{0}", count);

            }
        }

        private void metroLabel1_Click(object sender, EventArgs e)
        {

        }

        private void metroLabel2_Click(object sender, EventArgs e)
        {

        }

        private void metroLabel3_Click(object sender, EventArgs e)
        {

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void wskaznikTemp_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void labelProcesor_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
