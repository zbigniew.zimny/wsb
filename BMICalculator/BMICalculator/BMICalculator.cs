﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace BMICalculator
{
    public partial class BMICalculator : Form
    {
        public BMICalculator()
        {
            InitializeComponent();
            resultPanel.Visible = false;
        }

        private void BmiButton_Click(object sender, EventArgs e)
        {
            foreach (Control control in formPanel.Controls)
            {
                if (control is TextBox)
                {
                    if (IsEmptyValue(control.Text))
                    {
                        SetErrorEmptyField(control as TextBox);
                        ShowErrorMessage("Pola muszą być prawidłowo wypełnione");
                        control.Focus();
                        return;
                    }
                    else
                    {
                        RemoveError(control as TextBox);
                    }
                }
            }

            string enteredWeight = weight.Text;
            string enteredHeight = height.Text;

            double enteredWeightNumber;
            double enteredHeightNumber;

            if (!double.TryParse(enteredWeight, out enteredWeightNumber))
            {
                ShowErrorMessage(enteredWeight);
                return;
            }

            if (!double.TryParse(enteredHeight, out enteredHeightNumber))
            {
                ShowErrorMessage(enteredHeight);
                return;
            }

            double bmiValue = enteredWeightNumber / Math.Pow((enteredHeightNumber / 100), 2);
            resultScoreLabel.Text = bmiValue.ToString("#.##");

            AddResultScoreBackground(bmiValue);
            resultPanel.Visible = true;
        }

        private bool IsEmptyValue(string enteredValue)
        {
            return string.IsNullOrWhiteSpace(enteredValue);
        }

        private void ShowErrorMessage(string invalidValue)
        {
            MessageBox.Show("Wprowadzone dane są nieprawidłowe: " + invalidValue, "Błąd walidacji", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void AddResultScoreBackground(double bmiValue)
        {
            if (bmiValue < 18.5)
            {
                resultScoreLabel.BackColor = System.Drawing.Color.DeepSkyBlue;
            }
            else if (bmiValue >= 18.5 && bmiValue < 25)
            {
                resultScoreLabel.BackColor = System.Drawing.Color.SeaGreen;
            }
            else if (bmiValue >= 25 && bmiValue < 30)
            {
                resultScoreLabel.BackColor = System.Drawing.Color.Gold;
            }
            else if (bmiValue >= 30 && bmiValue < 35)
            {
                resultScoreLabel.BackColor = System.Drawing.Color.Orange;
            }
            else if (bmiValue >= 35)
            {
                resultScoreLabel.BackColor = System.Drawing.Color.Firebrick;
            }

        }

        private void ValidateEnteredValue(object sender, CancelEventArgs e)
        {
            TextBox textBox = sender as TextBox;

            if (IsEmptyValue(textBox.Text))
            {
                e.Cancel = true;
                SetErrorEmptyField(textBox);
            }
            else if (!ValidateNumber(textBox.Text))
            {
                e.Cancel = true;
                SetErrorNotNumberField(textBox);
            }
            else
            {
                RemoveError(textBox);
            }
        }

        private bool ValidateNumber(string value)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(value, "^[1-9][0-9]*$"))
            {
                return false;
            }
            return true;
        }

        private void SetErrorEmptyField(TextBox sender)
        {
            errorProvider1.SetError(sender, "Pole nie może być puste");
        }

        private void SetErrorNotNumberField(TextBox sender)
        {
            errorProvider1.SetError(sender, "Wprowadź prawidłową liczbę");
        }

        private void RemoveError(TextBox sender)
        {
            errorProvider1.SetError(sender, "");
        }

        private void BMICalculator_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;
        }
    }
}
